
clc
close all
clear all

%heightSet = [5 15 25 35 45];
experimentType = 'sim_ros_2obstacle';

gasReleaseRateType = 'fast'; %['constant', 'random']
experimentTime = [1 200];
posSet = ['y10'; 'y15'];
heightSet = [25, 75, 125];

experimentLabelPrefix = [gasReleaseRateType '_rate_pos'];

for i=1:size(posSet,1)
    for height=heightSet
        pos = posSet(i, :);
        inputPath = [experimentType '/' experimentLabelPrefix '_' pos ];
        outputPath = [inputPath '/' 'extracted_data'];
        if ~exist(outputPath, 'dir')
            mkdir(outputPath);
        end
        doProcessRosDataSets( gasReleaseRateType, pos, height, ...
            inputPath, outputPath, experimentTime );
    end
end

fprintf('Done! .... ');