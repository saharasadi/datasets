clc;
close all;
clear all;

sensorModel = 'TGS2620';

experimentType = 'sim_ros_2obstacle';
experimentLabel = 'mox_sensor_model_ppm';

filePath = [experimentType '/' experimentLabel '/' 'pos_y10' '/' 'conductanceMatrix_' sensorModel '.mat'];
dataStruct = load(filePath);
dataSource = dataStruct.conductanceMatrix_TGS2620;
dataSource = dataSource(dataSource(:,3) == 0.25, :);

startTime = 1;
endTime = 100;
indexSet = 1:size(dataSource,1);
inputData = [];

for t=startTime:endTime
   time = ones(size(dataSource, 1), 1) * t;
   windInfo = ones(size(dataSource, 1), 2) * (-1);
   data = [time, dataSource(indexSet,t+3), dataSource(indexSet,1:2), windInfo];
   inputData = [ inputData; data]; 
end

inputData(:, 2) = inputData(:,2);
%inputData(:,2) = inputData(:,2) / (4.06*10^(-17));
%inputData(inputData(:,2) < 10^(-40), 2) = 10^(-40);

% R_min = min(inputData(:,2));
% R_max = max(inputData(:,2));
% factor = R_min / (R_max - R_min);
% inputData(:,2) = factor * ((inputData(:,2).^(-1) * R_max) -1);
%inputData(inputData(:,2) > 10^5, 2) = 10^5;
%inputData(:,2) = log(inputData(:,2));