function [ ] = doProcessRosDataSets( gasReleaseRateType, pos, height, ...
            inputPath, outputPath, experimentTime )
%DOPROCESSROSDATASETS Summary of this function goes here
%   Detailed explanation goes here

%px       py      pz      concentration   u       v       w

startTime = experimentTime(1,1);
endTime = experimentTime(1,2);

prefixLabel = 'gasType0_simulation';
inputData = [];

for i=startTime:1:endTime 
    fprintf('height: %i, snapshot: %i\n', height, i);
    dataFilePath = [inputPath '/' '200_snapshots/' prefixLabel int2str(i) '.log'];
    rawData = load(dataFilePath);
    rawData = rawData(rawData(:,3) == height/100, :);
    dataSize = size(rawData, 1);
    dataset = zeros(dataSize, 6);
    dataset = [ones(dataSize, 1) * i, rawData(:,4), rawData(:,1), rawData(:,2), ... 
        rawData(:,5), rawData(:,6)];
    inputData = [inputData; dataset];
    clear rawData dataset
end

save([outputPath '/' 'release_rate_' gasReleaseRateType '_ethanol_' pos ...
    '_h_' int2str(height) 't_' int2str(startTime) '-' int2str(endTime) '.mat'], ...
    'inputData');
end

