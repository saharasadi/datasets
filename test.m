%DOPROCESSROSDATASETS Summary of this function goes here
%   Detailed explanation goes here

%px       py      pz      concentration   u       v       w

startTime = 5;
endTime = 5;

prefixLabel = 'gasType0_simulation';
experimentType = 'sim_ros_2obstacle';
inputData = [];

for i=startTime:1:endTime  
    dataFilePath = [experimentType '/' ...
        'test/' ...
        'example_ethanol_result/20_snapshots/' ...
        prefixLabel int2str(i) '.log'];
    rawData = load(dataFilePath);
    inputData = [rawData(:,4), rawData(:,1), rawData(:,2), ... 
        rawData(:,5), rawData(:,6)];

end

